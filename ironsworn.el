;;; package --- Ironsworn mode -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(defun ironsworn-roll-dN (faces)
  "Roll a die.
FACES: number of die faces."
  (+ 1 (random faces)))

(defun ironsworn-roll-NdN (dice-count dice-faces)
  "Roll multiple dice.
DICE-COUNT: number of dice
DICE-FACES: number of faces on each die"
  (let ((this-die (ironsworn-roll-dN dice-faces)))
    (if (> dice-count 0)
        (+ (ironsworn-roll-NdN (- dice-count 1) dice-faces) this-die)
      0)))

(defun ironsworn-eval-dice-formula (formula)
  "Evaluate a die-roll formula, eg: 1d100 .
FORMULA: the forumla to be evaluated"
  (let* ((dice-regex
          (rx bol
              (group (one-or-more digit))
              "d"
              (group (one-or-more digit))
              eol
              ))
         (dice-matches (s-match-strings-all dice-regex formula))
         (dice-groups (cdr (car dice-matches)))
         (dice-count (string-to-number (car dice-groups)))
         (dice-faces (string-to-number (nth 1 dice-groups))))

    (ironsworn-roll-NdN dice-count dice-faces)))

(defun ironsworn-eval-roll-result
    (action-or-progress challenge-die-1 challenge-die-2)
  "Evaluate the result of an action or progress roll.
ACTION-OR-PROGRESS: progress amount, or the action die roll + modifier
CHALLENGE-DIE-1: first challenge die
CHALLENGE-DIE-2: second challenge die"
  (concat
   (cond
    ((and (> action-or-progress challenge-die-1)
          (> action-or-progress challenge-die-2))
     "Strong Hit")

    ((or (> action-or-progress challenge-die-1)
         (> action-or-progress challenge-die-2))
     "Weak Hit")

    (t "Miss"))
   (if (= challenge-die-1 challenge-die-2) ", Match!" "")))

(defun ironsworn-roll-action (reason action-die-mod)
  "Calculate and insert an action roll.
REASON: reason for rolling
ACTION-DIE-MOD: action die modifier"
  (let* ((action-die (ironsworn-roll-dN 6))
         (challenge-die-1 (ironsworn-roll-dN 10))
         (challenge-die-2 (ironsworn-roll-dN 10))
         (action-die-roll (min 10 (+ action-die action-die-mod)))
         (result (ironsworn-eval-roll-result
                  action-die-roll
                  challenge-die-1
                  challenge-die-2)))
    (progn
      (insert
       (concat
        reason "\n"
        (number-to-string action-die-roll)
        " (" (number-to-string action-die)
        "+" (number-to-string action-die-mod) ")" " vs. "
        (number-to-string challenge-die-1) ", "
        (number-to-string challenge-die-2) " -> "
        result))
      (end-of-line)
      (newline-and-indent))))

(defun ironsworn-roll-action-query ()
  "Make an action roll, ask the user for reason and modifier."
  (interactive)
  (let ((reason (read-string "Reason for rolling: "))
        (action-die-mod (read-number "Enter modifier: ")))
    (ironsworn-roll-action reason action-die-mod)))

(defun ironsworn-roll-progress (reason progress)
  "Calculate and insert a progress roll.
REASON: reason for rolling
PROGRESS: current progress"
  (let* (
         (challenge-die-1 (ironsworn-roll-dN 10))
         (challenge-die-2 (ironsworn-roll-dN 10))
         (result (ironsworn-eval-roll-result
                  progress
                  challenge-die-1
                  challenge-die-2)))
    (progn
      (insert
       (concat
        reason "\n"
        (number-to-string progress) " vs. "
        (number-to-string challenge-die-1) ", "
        (number-to-string challenge-die-2) " -> "
         result))
      (end-of-line)
      (newline-and-indent))))

(defun ironsworn-roll-progress-query ()
  "Make a progress roll, ask the user for reason and progress."
  (interactive)
  (let ((reason (read-string "Reason for rolling: "))
        (progress (min 10 (read-number "Enter progress: "))))
    (ironsworn-roll-progress reason progress)))

(defun ironsworn-move (name stat)
  (let* ((action-die-mod
          (read-number (concat "Mod (" stat "+adds): "))))
    (ironsworn-roll-action name action-die-mod)))

(defun ironsworn-move-progress (name)
  (let* ((progress
          (read-number (concat "Progress: "))))
    (ironsworn-roll-progress name progress)))


;; MOVES

;; ADVENTURE MOVES

(defun ironsworn-sf-move-face-danger ()
  (interactive)
  (ironsworn-move "Face Danger" "any"))

(defun ironsworn-sf-move-secure-an-advantage ()
  (interactive)
  (ironsworn-move "Secure An Advantage" "any"))

(defun ironsworn-sf-move-gather-information ()
  (interactive)
  (ironsworn-move "Gather Information" "wits"))

(defun ironsworn-sf-move-compel ()
  (interactive)
  (ironsworn-move "Compel" "heart/iron/shadow"))

;; QUEST MOVES

(defun ironsworn-sf-move-swear-an-iron-vow ()
  (interactive)
  (ironsworn-move "Swear An Iron Vow" "heart"))

(defun ironsworn-sf-move-fulfill-your-vow ()
  (interactive)
  (ironsworn-move-progress "Fulfill Your Vow"))

;; CONNECTION MOVES

(defun ironsworn-sf-move-make-a-connection ()
  (interactive)
  (ironsworn-move "Make A Connection" "heart"))

(defun ironsworn-sf-move-forge-a-bond ()
  (interactive)
  (ironsworn-move-progress "Forge A Bond"))

(defun ironsworn-sf-move-test-your-relationship ()
  (interactive)
  (ironsworn-move "Test Your Relationship" "heart"))

;; EXPLORATION

(defun ironsworn-sf-move-undertake-an-expedition ()
  (interactive)
  (ironsworn-move "Undertake An Expedition" "edge/shadow/wits"))

(defun ironsworn-sf-move-explore-a-waypoint ()
  (interactive)
  (ironsworn-move "Explore A Waypoint" "wits"))

(defun ironsworn-sf-move-finish-an-expedition ()
  (interactive)
  (ironsworn-move-progress "Finish An Expedition"))

(defun ironsworn-sf-move-set-a-course ()
  (interactive)
  (ironsworn-move "Set A Course" "supply"))

(defun ironsworn-sf-move-make-a-discovery ()
  (interactive)
  (insert (concat "Make a Discovery" "\n" (number-to-string (ironsworn-roll-NdN 1 100)))))

(defun ironsworn-sf-move-confront-chaos ()
  (interactive)
  (insert (concat "Confront Chaos" "\n" (number-to-string (ironsworn-roll-NdN 1 100)))))

;; COMBAT

(defun ironsworn-sf-move-enter-the-fray ()
  (interactive)
  (ironsworn-move "Enter the Fray" "any"))

(defun ironsworn-sf-move-gain-ground ()
  (interactive)
  (ironsworn-move "Gain Ground" "any"))

(defun ironsworn-sf-move-react-under-fire ()
  (interactive)
  (ironsworn-move "React Under Fire" "any"))

(defun ironsworn-sf-move-strike ()
  (interactive)
  (ironsworn-move "Strike" "edge/iron"))

(defun ironsworn-sf-move-clash ()
  (interactive)
  (ironsworn-move "Clash" "edge"))

(defun ironsworn-sf-move-battle ()
  (interactive)
  (ironsworn-move "Battle" "any"))

(defun ironsworn-sf-move-take-decisive-action ()
  (interactive)
  (ironsworn-move-progress "Take Decisive Action"))

;; THRESHOLD MOVES

(defun ironsworn-sf-move-face-death ()
  (interactive)
  (ironsworn-move "Face Death" "heart"))

(defun ironsworn-sf-move-face-desolation ()
  (interactive)
  (ironsworn-move "Face Desolation" "heart"))

(defun ironsworn-sf-move-overcome-destruction ()
  (interactive)
  (ironsworn-move-progress "Overcome Destruction"))

;; RECOVER MOVES

(defun ironsworn-sf-move-sojourn ()
  (interactive)
  (ironsworn-move "Sojourn" "heart"))

(defun ironsworn-sf-move-heal ()
  (interactive)
  (ironsworn-move "Heal" "iron/wits/heart"))

(defun ironsworn-sf-move-hearten ()
  (interactive)
  (ironsworn-move "Hearten" "heart"))

(defun ironsworn-sf-move-repair ()
  (interactive)
  (ironsworn-move "Repair" "wits/supply"))

(defun ironsworn-sf-move-resupply ()
  (interactive)
  (ironsworn-move "Repair" "heart/iron/shadow/wits"))

;; LEGACY MOVES

(defun ironsworn-sf-move-continue-a-legacy ()
  (interactive)
  (ironsworn-move-progress "Continue A Legacy"))


;; FATE MOVES

(defun ironsworn-sf-move-pay-the-price ()
  (interactive)
  (insert (concat "Pay the Price" "\n" (number-to-string (ironsworn-roll-NdN 1 100)))))

(defun ironsworn-sf-move-ask-the-oracle ()
  (interactive)
  (insert (concat "Ask The Oracle" "\n" (number-to-string (ironsworn-roll-NdN 1 100)))))


;;;###autoload
(define-minor-mode ironsworn-mode
  "Toggle ironsworn mode"
  :lighter " Ironsworn"
  :keymap
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-r") 'ironsworn-roll-action-query)
    (define-key map (kbd "C-c C-p") 'ironsworn-roll-progress-query)
    map))

(provide 'ironsworn)
;;; ironsworn.el ends here
