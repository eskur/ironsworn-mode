;;; ironsworn-oracles.el --- description -*- lexical-binding: t; -*-
;;
;;; Commentary:
;;
;;; Code:
(require 'json)
(require 'rx)

(defconst path-oracles-data-dir (file-name-directory load-file-name))

;; (defun ironsworn-parse-result-range (range-str)
;;   "Parse a result key range, eg: 95-100 -> (95, 100)
;; RANGE-STR: the string to be parsed"
;;   (let* ((range-regex
;;           (rx bol
;;               (group (one-or-more digit))
;;               "-"
;;               (group (one-or-more digit))
;;               eol
;;               ))
;;          (matches (s-match-strings-all range-regex range-str))
;;          (match-groups (cdr (car matches)))
;;          )
;;     (mapcar 'string-to-number match-groups)))

;; (defun string-comp (str1 str2)
;;   "String equality.
;; STR1: first string
;; STR2: second string"
;;   (eq (compare-strings str1 nil nil str2 nil nil) t))

;; (defun ironsworn-oracle-roll-uniform (entries roll)
;;   "Roll on a uniform oracle table.
;; ENTRIES: a list of table entries
;; ROLL: the index to use"
;;   (gethash (number-to-string roll) entries))

;; (defun roll-in-range (range roll)
;;   "Determine if a roll is withing a given range key.
;; RANGE: the range to search
;; ROLL: the value"
;;   (let* (
;;          (range-split (split-string range "-"))
;;          (range-low (string-to-number (car range-split)))
;;          (range-high (string-to-number (car (cdr range-split))))
;;          )
;;     (<= range-low roll range-high)))

;; (defun ironsworn-oracle-roll-segmented (entries roll)
;;   "Roll on a segmented oracle table.
;; ENTRIES: a list of table entries
;; ROLL: the key to use"
;;   (let* (alist)
;;     (maphash (lambda (key val)
;;                (if (roll-in-range key roll) (push (cons key val) alist)))
;;              entries)
;;     (cdr (car alist))))

;; (defun ironsworn-oracle-roll (oracle-name)
;;   "Roll on an oracle oracle.
;; ORACLE-NAME: name of the oracle"
;;   (let* ((json-object-type 'hash-table)
;;          (json-array-type 'list)
;;          (json-key-type 'string)
;;          (json (json-read-file (expand-file-name "./ironsworn-oracles.json" path-oracles-data-dir)))
;;          (oracle (gethash oracle-name json))
;;          (formula (gethash "formula" oracle))
;;          (type (gethash "type" oracle))
;;          (entries (gethash "entries" oracle))
;;          (roll (ironsworn-eval-dice-formula formula)))
;;     (progn
;;       (insert
;;        (cond
;;         ((string-comp type "uniform") (ironsworn-oracle-roll-uniform entries roll))
;;         ((string-comp type "segmented") (ironsworn-oracle-roll-segmented entries roll))
;;         (t "ERROR: UNKNOWN ORACLE TYPE")
;;         )
;;        "\n"))))

;; (defun ironsworn-oracle-action ()
;;   "Roll on the action oracle."
;;   (interactive)
;;   (ironsworn-oracle-roll "action"))

;; (defun ironsworn-oracle-theme ()
;;   "Roll on the theme oracle."
;;   (interactive)
;;   (ironsworn-oracle-roll "theme"))

;; (defun ironsworn-oracle-major-plot-twist ()
;;   "Roll on the major plot twist oracle."
;;   (interactive)
;;   (ironsworn-oracle-roll "major-plot-twist"))

;; (defun ironsworn-oracle-character-gender ()
;;   "Roll on the character gender oracle."
;;   (interactive)
;;   (ironsworn-oracle-roll "character-gender"))

;; (defun ironsworn-oracle-character-descriptor ()
;;   "Roll on the character descriptor oracle."
;;   (interactive)
;;   (ironsworn-oracle-roll "character-descriptor"))

;; (defun ironsworn-oracle-character-goals ()
;;   "Roll on the character goals oracle."
;;   (interactive)
;;   (ironsworn-oracle-roll "character-goals"))



(provide 'ironsworn-oracles)
;;; ironsworn-oracles.el ends here
